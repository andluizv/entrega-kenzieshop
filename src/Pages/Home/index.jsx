import { useSelector } from "react-redux";
import ProductCard from '../../components/ProductCard'


const HomePage = () => {
    const { products } = useSelector((store) => store);
	return (
		<div className="marge">
			<div className="containerList">
				{products.map((item) => (
					<ProductCard key={'@home'+item.id} product={item} />
				))}
			</div>
		</div>
	);
};

export default HomePage;
