import styled from "styled-components";

export const ContainerList = styled.div`
    display: flex;
    flex-wrap: wrap;
    width: 75%;
`