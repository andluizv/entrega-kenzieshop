import { useSelector } from "react-redux";
import ProductCard from "../../components/ProductCard";
import CheckOut from "../../components/CheckOut";

const CartPage = () => {
    const {cart} = useSelector(store=>store);
	const show = () => {
        let output = [];
        let auxFor =[];
        cart.forEach(item => {
            if(auxFor.includes(item.id)){
                return
            }
            auxFor.push(item.id);
            output.push(item);
        })
        
        return output;
    }
    return (
		<div className="marge">
			<div className="containerList">
				{cart.length > 0? 
					show().map((item) =>  
						        <ProductCard key={'@cart' + item.id} product={item} cart amount={cart.filter(arrCh=>arrCh.id === item.id).length}/>
                    )
				 : (
					<h1>Você ainda não adicionou nada ao carrinho</h1>
				)}
			</div>
            {cart.length > 0 ? <CheckOut /> : null}
		</div>
	);
};

export default CartPage;
