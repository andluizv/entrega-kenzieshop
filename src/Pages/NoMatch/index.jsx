const Page404 = () => {
    return ( 
        <div style={{display: 'grid', placeItems: 'center'}}>
            <h1>Ops! Página não encontrada!</h1>
            <h2>Erro: 404!</h2>
            <h1>Talvez esteja em contrução?</h1>
        </div>
     );
}
 
export default Page404;