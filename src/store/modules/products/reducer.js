// import img from '../../../img/products/'

const defaultState = [
    {id: 1, name: 'Pampers M Confort Sec c/280 un.', img: './img/products/pampers280m.webp', price: 214.90},
    {id: 2, name: 'Pampers G Confort Sec c/240 un.', img: './img/products/pampers240g.webp', price: 214.90},
    {id: 3, name: 'Pampers P Confort Sec c/288 un.', img: './img/products//pampers288p.webp', price: 214.90},
    {id: 4, name: 'Pampers M SuperSec c/52 un.', img: './img/products//pampers52m.webp', price: 39.59},
    {id: 5, name: 'Pampers G SuperSec c/80 un.', img: './img/products//pampers80g.webp', price: 61.90},
    {id: 6, name: 'Pampers P SuperSec c/34 un.', img: './img/products//pampers34p.webp', price: 18.89},
    {id: 7, name: 'Huggies Supreme Care M c/320 un.', img: './img/products//huggies320m.webp', price: 239.90},
    {id: 8, name: 'Huggies Supreme Care G c/192 un.', img: './img/products//huggies192g.webp', price: 172.35},
    {id: 9, name: 'Huggies Supreme Care P c/192 un.', img: './img/products//huggies192p.webp', price: 126.00},
    {id: 10, name: 'Huggies Supreme Care P c/48 un.', img: './img/products//huggies48p.webp', price: 32.50},
    {id: 11, name: 'Huggies Roupinha Supreme Care G c/60 un.', img: './img/products//huggies60g.webp', price: 62.99},
    {id: 12, name: 'Huggies Supreme Care M c/80 un.', img: './img/products//huggies320m.webp', price: 57.45},
]

const productsReducer = (state = defaultState) => {
    return state;
}

export default productsReducer;