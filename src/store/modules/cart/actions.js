
export const addProduct = (add) => ({
    type: '@cart_ADD',
    add
})
export const removeProduct = (remove) => ({
    type: '@cart_REMOVE',
    remove
})