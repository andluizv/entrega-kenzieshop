import { addProduct, removeProduct } from "./actions";

export const cartAddThunk = (product) => (dispatch, getState) => {
    const  {cart} = getState();
    const newList = [...cart, product];
    localStorage.setItem("tmpCart", JSON.stringify(newList));
    dispatch(addProduct(newList));
}

export const cartRemoveThunk = (id) => (dispatch, getState) => {
    const { cart } = getState();
    const newList = cart.filter(item=>item.id !== id);
    localStorage.setItem("tmpCart", JSON.stringify(newList));
    dispatch(removeProduct(newList));
}

export const cartRemoveONEThunk = (product) => (dispatch, getState) => {
    const { cart } = getState();
    const tmpList = [...cart]
    tmpList.splice(cart.indexOf(product),1);
    localStorage.setItem("tmpCart", JSON.stringify(tmpList));
    dispatch(removeProduct(tmpList));
}
