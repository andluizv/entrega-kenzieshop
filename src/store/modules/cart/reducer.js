const defaultState = JSON.parse(localStorage.getItem('tmpCart')) || [];

const cartReducer = (state = defaultState, action) => {
    switch (action.type) {
        case '@cart_ADD':
            return action.add;
    
        case '@cart_REMOVE' :
            return action.remove;
        
        default:
            return state;
    }
}

export default cartReducer;