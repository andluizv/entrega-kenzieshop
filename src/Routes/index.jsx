import { Switch, Route } from 'react-router-dom';
import HomePage from '../Pages/Home';
import CartPage from '../Pages/Cart';
import TopBar from '../components/TopBar';
import Page404 from '../Pages/NoMatch';

const Routes = () => {

    return (
		<Switch>
			<Route exact path="/">
				<TopBar />
				<HomePage />
			</Route>
			<Route path="/cart">
				<TopBar />
				<CartPage />
			</Route>
            <Route path="*">
                <Page404 />
            </Route>
		</Switch>
	);
}

export default Routes;