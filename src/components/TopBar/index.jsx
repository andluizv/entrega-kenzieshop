import { AppBar, Toolbar, Typography, Button, Badge } from "@material-ui/core";
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from "react-redux";
import { ShoppingCart,  ArrowBack} from '@material-ui/icons';
import { useLocation, useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  menuButton: {
    marginLeft: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  pointer: {
      cursor: 'pointer',
  }
  
}));

const TopBar = ({cartPage}) => {
    const location = useLocation();
    const history = useHistory();
    const classes = useStyles();
    const { cart } = useSelector(store=>store);

    const pageVerify = () => {
        if (location.pathname === '/'){
            return true;
        }
        return false;
    }
    const cartBadge = (
		<Badge color="secondary" onClick={()=> history.push('/cart')} className={classes.pointer} badgeContent={cart.length && cart.length}>
			<ShoppingCart />
		</Badge>
	);
    return (
		<AppBar color="primary" position="fixed">
			<Toolbar>
				<Typography className={classes.title} variant="h6">
					Kenzie Shop
				</Typography>
				{pageVerify() ? (
					cartBadge
				) : (
					<Button startIcon={<ArrowBack />} onClick={()=> history.push('/')} color='inherit'>Voltar</Button>
				)}

				<Button className={classes.menuButton} onClick={()=> history.push('/signin')} color="inherit">
					Login
				</Button>
			</Toolbar>
		</AppBar>
	);
}
 
export default TopBar;