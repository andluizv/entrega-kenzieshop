import styled from "styled-components";

export const Image = styled.img`
    width: 200px;
`

export const Container = styled.div`
    box-shadow: 0 0 2px 0px black;
    width: clamp(190px, 16.5vw, 300px);
    padding: 1rem;
    display: grid;
    place-items: center;
    
`

export const Amounter = styled.span`
    padding: 1rem;
`