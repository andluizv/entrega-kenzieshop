import { Image, Container, Amounter } from './styles';
import { Button } from "@material-ui/core";
import { Add, Remove } from '@material-ui/icons';
import { useDispatch } from 'react-redux';
import { cartAddThunk, cartRemoveThunk, cartRemoveONEThunk} from '../../store/modules/cart/thunks'


const ProductCard = ({product, cart, amount}) => {
    const dispatch = useDispatch();
    
    const handleAdd = () => {
        dispatch(cartAddThunk(product))
    }

    const addRemove = (
		<div className="centered">
			<div className='plusminus'>
				<Button variant="contained" size="small" onClick={handleAdd}>
					<Add />
				</Button>
				<Amounter>{amount}</Amounter>
				<Button
					variant="contained"
					size="small"
					onClick={() => dispatch(cartRemoveONEThunk(product))}
				>
					<Remove />
				</Button>
			</div>
			<br />
			{"R$ " + (product.price * amount).toFixed(2)}
		</div>
	);

    return (
        <Container>
            <Image src={product.img} alt={product.name} />
            <h3>{product.name}</h3>
            <h4>{amount? addRemove : 'R$ ' + product.price}</h4>
            {cart? <Button variant='contained' onClick={()=>dispatch(cartRemoveThunk(product.id))}>Remover</Button> : <Button variant='contained' onClick={handleAdd}>Adicionar</Button>}
        </Container>
    )
}

export default ProductCard;