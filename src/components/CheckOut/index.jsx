import { Button } from "@material-ui/core"
import { useSelector } from "react-redux";
import { Floating } from './styles'
import { useHistory } from "react-router";

const CheckOut = () => {
    const { cart } = useSelector(store=>store);
    const history = useHistory();

    return (
		<Floating>
			<h3>Resumo do pedido:</h3>
			<p>
				{cart.length} produtos no total de: R${" "}
				{cart.reduce(
					(acc, item) =>
						Number(Number(acc) + Number(item.price)).toFixed(2),
					0
				)}
			</p>
			<Button
				variant="contained"
				color="primary"
				onClick={() => history.push("/signin")}
			>
				Finalizar Pedido
			</Button>
		</Floating>
	);
}
 
export default CheckOut;